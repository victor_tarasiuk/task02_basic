package com.epam.task;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 08.11.2019
 *
 */
public class OddAndEvenNumber {
    private int sumOdd = 0;
    private int sumEven = 0;

    /**
     * Created method for get odd numbers from numberOne to numberTwo.
     */
      public final void getOddNumbersFromNumberOneToNumberTwo(
              final int numberOne, final int numberTwo) {
        System.out.printf("%n");
        System.out.print("The odd numbers are from numberOne to numberTwo: ");

        for (int i = numberOne; i <= numberTwo; i++) {

            if (i % 2 != 0) {
                System.out.print(i + " ");
            }

        }

    }

    /**
     * Created method for get odd numbers from numberTwo to numberOne.
     */
    public final void getOddNumbersFromNumberTwoToNumberOne(
            final int numberOne, final int numberTwo) {
        System.out.printf("%n");
        System.out.print("The odd numbers are from numberTwo to numberOne: ");

        for (int i = numberTwo; i >= numberOne; i--) {

            if (i % 2 != 0) {
                System.out.print(i + " ");
            }
        }
        System.out.printf("%n");
    }

    /**
     * Created method for get sum odd numbers from numberOne to numberTwo.
     */
    public final void getSumFOddNumbersFromNumberOneToNumberTwo(
            final int numberOne, final int numberTwo) {

        for (int i = numberOne; i <= numberTwo; i++) {

            if (i % 2 != 0) {
                sumOdd += i;
            }

        }
        System.out.println("Sum odd numbers are from numberOne to numberTwo: "
                + sumOdd);

    }

    /**
     * Created method for get sum even numbers from numberOne to numberTwo.
     */
    public final void getSumFEvenNumbersFromNumberOneToNumberTwo(
            final int numberOne, final int numberTwo) {

        for (int i = numberOne; i <= numberTwo; i++) {

            if (i % 2 == 0) {
                sumEven += i;
            }

        }
        System.out.println("Sum even numbers are from numberOne to numberTwo: "
                + sumEven);

    }

}



