package com.epam.task;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 08.11.2019
 *
 */
public class FibanacciNumber {
    public static final int NUMBER_ONE = 1;
    public static final int NUMBER_TWO = 2;
    public static final int NUMBER_FIVE = 5;
    public static final int NUMBER_HUNDRED = 100;

    private int[] arrayFib;
    private int countOdd = 0;
    private int countEven = 0;
    private double percentageOdd;
    private double percentageEven;
    private int fibNumber;
    private int maxOdd;
    private int maxEven;

    /**
     * Created method for get Fibanacci Numbers from number.
     */
    public final void getFibanacciNumbersFromNumber(final int number) {
        arrayFib = new int[number];
        System.out.print("Fibanacci Numbers from number (" + number + ") : ");

        for (int i = 0; i < number; i++) {
            // Using direct formula
            fibNumber = (int) ((Math.pow((NUMBER_ONE
                    + Math.sqrt(NUMBER_FIVE)), i)
                    - Math.pow((NUMBER_ONE - Math.sqrt(NUMBER_FIVE)), i))
                    / (Math.pow(NUMBER_TWO, i) * Math.sqrt(NUMBER_FIVE)));

            System.out.print(fibNumber + " ");

            arrayFib[i] = fibNumber;
        }
        System.out.printf("%n");

    }

    /**
     * Created method for get max odd Fibanacci Numbers from number.
     */
    public final void getMaxOddFibanacciNumbersFromNumber() {
        maxOdd = arrayFib[0];

        for (int i = 0; i < arrayFib.length; i++) {

            if ((maxOdd < arrayFib[i]) & (arrayFib[i] % 2 != 0)) {
                maxOdd = arrayFib[i];
            }

        }

        System.out.println("Max odd Fibanacci Numbers from number: "
                + maxOdd);
    }

    /**
     * Created method for get max even Fibanacci Numbers from number.
     */
    public final void getMaxEvenFibanacciNumbersFromNumber() {
        maxEven = arrayFib[0];

        for (int i = 0; i < arrayFib.length; i++) {

            if ((maxEven < arrayFib[i]) & (arrayFib[i] % 2 == 0)) {
                maxEven = arrayFib[i];
            }

        }

        System.out.println("Max even Fibanacci Numbers from number: "
                + maxEven);
    }

    /**
     * Created method for get percentage odd Fibanacci Numbers from number.
     */
    public final void getPercentageOddFibanacciNumbersFromNumber() {

        for (int i = 0; i < arrayFib.length; i++) {

            if (arrayFib[i] % 2 != 0) {
                countOdd++;
            }

        }
        percentageOdd = (double) (countOdd * NUMBER_HUNDRED) / arrayFib.length;
        System.out.println("Percentage odd Fibanacci Numbers from number: "
                + percentageOdd + " %");
    }

    /**
     * Created method for get percentage even Fibanacci Numbers from number.
     */
        public final void getPercentageEvenFibanacciNumbersFromNumber() {

            for (int i = 0; i < arrayFib.length; i++) {

                if (arrayFib[i] % 2 == 0) {
                    countEven++;
                }
            }

            percentageEven = (double) (countEven * NUMBER_HUNDRED)
                    / arrayFib.length;
            System.out.println("Percentage even Fibanacci Numbers from number: "
                    + percentageEven + " %");

        }
    }


