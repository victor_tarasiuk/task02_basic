package com.epam;

import java.util.Scanner;
import com.epam.task.FibanacciNumber;
import com.epam.task.OddAndEvenNumber;

/**
 * @author Victor Tarasiuk
 * @version 1.0
 * @since 08.11.2019
 *
 */
public class Application {
    public static void main(String[] args) {

        /**
         * Created Scaner to set numbers.
         */
        Scanner scanner = new Scanner(System.in);
        int numberOne = scanner.nextInt();
        int numberTwo = scanner.nextInt();

        /**
         * Creates object OddAndEvenNumber and insert all methods to execute.
         */
        OddAndEvenNumber oddNumber = new OddAndEvenNumber();
        oddNumber.getOddNumbersFromNumberOneToNumberTwo(numberOne,
                numberTwo);
        oddNumber.getOddNumbersFromNumberTwoToNumberOne(numberOne,
                numberTwo);
        oddNumber.getSumFEvenNumbersFromNumberOneToNumberTwo(numberOne,
                numberTwo);
        oddNumber.getSumFOddNumbersFromNumberOneToNumberTwo(numberOne,
                numberTwo);

        /**
         * Created Scaner to set numbers
         */
        scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        /**
         * Creates object FibanacciNumber and insert all methods to execute.
         */
        FibanacciNumber fibanacciNumber = new FibanacciNumber();
        fibanacciNumber.getFibanacciNumbersFromNumber(number);
        fibanacciNumber.getMaxOddFibanacciNumbersFromNumber();
        fibanacciNumber.getMaxEvenFibanacciNumbersFromNumber();
        fibanacciNumber.getPercentageOddFibanacciNumbersFromNumber();
        fibanacciNumber.getPercentageEvenFibanacciNumbersFromNumber();
    }
}